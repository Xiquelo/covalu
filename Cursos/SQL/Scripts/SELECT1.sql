select * from personal 

SELECT ApellidoPaterno + ' ' + ApellidoMaterno + ' ' + Nombre,
LTRIM (ApellidoPaterno + ' ' + ApellidoMaterno + ' ' + Nombre)

from Personal

SELECT LTRIM('       Hola')
SELECT RTRIM('             HOLA   ')

SELECT ApellidoPaterno + ' ' + ApellidoMaterno + ' ' + Nombre as 'Concat Consulta',
SUBSTRING ((ApellidoPaterno + ' ' + ApellidoMaterno + ' ' + Nombre), 1, 15) as 'Substring Consulta',
LTRIM (SUBSTRING (('      ' + ApellidoPaterno + ' ' + ApellidoMaterno + ' ' + Nombre), 5, 12)) as 'Len Consulta',
REPLACE (REPLACE (ApellidoPaterno + ' ' + ApellidoMaterno + ' ' + Nombre, 'a', '='), 'Gutierrez', 'z') as 'Replace Consulta',
LOWER (ApellidoPaterno + ' ' + ApellidoMaterno + ' ' + Nombre) as 'Lower Consulta',
UPPER (ApellidoPaterno + ' ' + ApellidoMaterno + ' ' + Nombre) as 'Upper Consulta',
LEN (ApellidoPaterno + ' ' + ApellidoMaterno + ' ' + Nombre) as 'Len Consulta'
FROM Personal

SELECT FechaAlta, * FROM Personal
WHERE FechaAlta BETWEEN '2000-01-01' and '2022-01-01'  ---74

SELECT FechaAlta, * FROM Personal ---151 

SELECT * FROM Personal
WHERE ApellidoPaterno like('%h%')

---Numericos
SELECT PRECIO, * FROM VENTAD

SELECT AVG(Precio) AS PROMEDIO, MAX(PRECIO) AS 'PRECIO MAXIMO', MIN(PRECIO) AS 'PRECIO MINIMO' FROM VENTAD

SELECT COUNT (PRECIO) FROM VENTAD
WHERE PRECIO BETWEEN 4 AND 5

---FUNCIONES FLECHA
SELECT GETDATE()

SELECT DATENAME (YEAR, GETDATE()) AS 'AÑO'
SELECT DATENAME (MONTH, GETDATE()) AS 'MES'
SELECT DATENAME (DAYOFYEAR, GETDATE()) AS 'DIA DEL AÑO'
SELECT DATENAME (DAY, GETDATE()) AS 'DIA'
SELECT DATENAME (WEEK, GETDATE()) AS 'SEMANA'
SELECT DATENAME (WEEKDAY, GETDATE()) AS 'DIA DE LA SEMANA'

SELECT DATEPART (YEAR, GETDATE()) AS 'AÑO'
SELECT DATEPART (MONTH, GETDATE()) AS 'MES'
SELECT DATEPART (DAYOFYEAR, GETDATE()) AS 'DIA DEL AÑO'
SELECT DATEPART (DAY, GETDATE()) AS 'DIA'
SELECT DATEPART (WEEK, GETDATE()) AS 'SEMANA'
SELECT DATEPART (WEEKDAY, GETDATE()) AS 'DIA DE LA SEMANA'

SELECT DAY(GETDATE())
SELECT MONTH(GETDATE())
SELECT YEAR(GETDATE())

SELECT DATEDIFF(DAY, GETDATE(), '2000-01-01')

SELECT DATEADD (DAY, 1, GETDATE())