--Ejemplo Select simple
SELECT * FROM Venta

--Ejemplo select con filtros
SELECT ID, Empresa, Mov, Ejercicio, Periodo
FROM   Venta
WHERE  Mov = 'Factura' AND Ejercicio = 2021 AND PERIODO = 1

SELECT * 
FROM Venta
WHERE Mov IN ('Devolucion MCC','Devolucion Secion C','Devolucion Venta')

SELECT * 
FROM Venta
WHERE Mov LIKE ('Dev%')

--Ejemplo select ordenado.
SELECT Cliente, Nombre, Pais
FROM Cte
ORDER BY Cliente DESC

--DISTINCT
SELECT DISTINCT Mov FROM Venta --Correcto

SELECT DISTINCT ID FROM Venta  --No Correcto



-- Ejercicio 1
SELECT SalesOrderID, OrderDate, Status
FROM sales.SalesOrderHeader

--Ejercicio 2
SELECT SalesOrderID, OrderDate, Status
FROM sales.SalesOrderHeader
ORDER BY OrderDate DESC

--Ejercicio 3
SELECT DISTINCT TerritoryID FROM Sales.SalesOrderHeader

--Ejercicio 4
--Mostrar el n�mero de registros de la tabla Employee proveniente del esquema HumanResources donde el JobTitle sea Database Administrator .
SELECT * 
FROM HumanResources.Employee
WHERE JobTitle = 'Database Administrator'


-- Funciones Fecha

SELECT GETDATE()				--Nos devuelve la fecha del sistema  en donde se encuentra el servidor de SQL
SELECT SYSDATETIMEOFFSET()		
SELECT SYSUTCDATETIME()


SELECT YEAR(GETDATE())			--Nos devuelve el a�o del parametro de fecha que se le asigne
SELECT YEAR(FechaEmision), * FROM Venta
SELECT MONTH(GETDATE())			--Nos devuelve el mes del paramtetro de fecha que se le asigne
SELECT MONTH(FechaEmision), * FROM Venta

SELECT * FROM Venta WHERE YEAR(FechaEmision) = 2021 AND MONTH(FechaEmision) = 1


--Funciones de agregaci�n
SELECT Estatus, SUM(importe)
FROM Venta
GROUP BY Estatus

SELECT Ejercicio, SUM(importe)
FROM Venta
GROUP BY Ejercicio
ORDER BY 1

SELECT Estatus, COUNT(*)
FROM Venta
GROUP BY Estatus



