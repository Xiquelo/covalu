USE DMI

/*Sintaxis INNER JOIN*/
SELECT 
	Venta.ID,
	Venta.FechaEmision,
	Venta.Mov,
	VentaD.ID,
	VentaD.Articulo,
	VentaD.Cantidad,
	VentaD.Precio
FROM 
	Venta
INNER JOIN 
	VentaD ON Venta.ID = VentaD.ID
WHERE 
	VentaD.Articulo = 'ARTSER404001'
ORDER BY
	FechaEmision

/*Sintaxis INNER JOIN con identificador de tabla*/
SELECT 
	v.ID,
	v.FechaEmision,
	v.Mov,
	d.ID,
	d.Articulo,
	d.Cantidad,
	d.Precio
FROM 
	Venta v
INNER JOIN 
	VentaD d ON v.ID = d.ID
WHERE 
	d.Articulo = 'ARTSER404001'
ORDER BY
	FechaEmision

/*Sintaxis INNER JOIN con campo �nico existente en la tabla*/
SELECT 
	v.ID,
	FechaEmision,
	v.Mov,
	d.ID,
	Articulo,
	Cantidad,
	d.Precio
FROM 
	Venta v
INNER JOIN 
	VentaD d ON v.ID = d.ID
WHERE 
	Articulo = 'ARTSER404001'
ORDER BY
	v.FechaEmision

/*INNER JOIN por m�s de dos tablas*/
SELECT 
	v.ID,
	FechaEmision,
	v.Mov,
	d.ID,
	d.Articulo,
	a.Descripcion1,
	Cantidad,
	d.Precio
FROM 
	Venta v
INNER JOIN 
	VentaD d ON v.ID = d.ID
INNER JOIN
	Art a ON d.Articulo = a.Articulo
ORDER BY
	v.FechaEmision

/*Ejercicio hacer relaci�n entre las tablas Venta, VentaD, Art, Alm*/

SELECT
	*
FROM
	Venta v
INNER JOIN
	VentaD d ON v.ID = d.ID
INNER JOIN
	Art a ON d.Articulo = a.Articulo
INNER JOIN
	Alm alm ON alm.Almacen = d.Almacen

/*Sintaxis LEFT JOIN*/
USE tallerSQL
SELECT
	*
FROM
	Empleado e
LEFT JOIN
	Venta v ON e.ID = v.Empleado

/*Sintaxis LEFT JOIN informaci�n de tabla con identificador*/
SELECT
	e.*,
	v.Importe
FROM
	Empleado e
LEFT JOIN
	Venta v ON e.ID = v.Empleado

SELECT
	e.Nombre,
	v.*
FROM
	Empleado e
LEFT JOIN
	Venta v ON e.ID = v.Empleado

/*Ejercicio*/
USE DMI
SELECT  
	ID,
	Empresa,
	Mov, 
	FechaEmision
FROM 
	Venta
WHERE
		Empresa = 'C1001' 
	AND Ejercicio = 2021 AND Periodo = 1


--Tarea
/*Muestra el ID, Empresa, Mov y FechaEmision de la tabla Venta, el Art�culo proveniente de la tabla VentaD 
sea ARTSER404001 incluyendo el campo Descripcion1  de la tabla de Art con fecha del 01 de Marzo al 31 de Marzo del a�o 2021 Sobre la base DMI*/


SELECT 
	V.ID,
	V.EMPRESA,
	V.MOV,
	V.FECHAEMISION,
	D.ARTICULO,
	A.DESCRIPCION1
FROM 
	VENTA
INNER JOIN
	VENTA V ON V.ID = D.ID
INNER JOIN
	ART A ON 
WHERE


/*Muestra el ID, Empresa, Mov y FechaEmision de la tabla Venta, el Art�culo proveniente de la tabla VentaD 
y que sea 00003, incluyendo el campo Descripcion1  de la tabla de Art con fecha del 01 de Marzo al 31 de Marzo del a�o 2021 Sobre la base Tolukasa*/

SELECT
	V.ID,
	V.EMPRESA,
	V.MOV,
	V.FECHAEMISION,
	D.ARTICULO,
	A.DESCRIPCION1
FROM VENTA V
INNER JOIN
	VENTAD D ON V.ID = D.ID
INNER JOIN
	ART A ON A.ARTICULO = D.ARTICULO
WHERE
	D.ARTICULO = '00003'
	AND V.FECHAEMISION BETWEEN '2020-01-01' AND '2020-01-31'
ORDER BY V.FECHAEMISION