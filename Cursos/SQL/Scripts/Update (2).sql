-- Crear base de datos para ejercicios
CREATE DATABASE talleSQL2022 

--Poner en uso la base de datos
USE talleSQL2022

--Crear tabla para primer ejemplo
CREATE TABLE personal (
	ID			INT IDENTITY(1,1),
	Nombre		VARCHAR(20),
	Color		VARCHAR(20),
	FechaAlta	DATETIME,
	Estatus		CHAR
)

--A�adir default al campo fechaAlta con la fecha del sistema
ALTER TABLE personal ADD DEFAULT GETDATE() FOR fechaAlta

--Insertamos registro
INSERT INTO personal (Nombre, Color, Estatus) VALUES ('Juan', 'Verde', 'A'), ('Pedro', 'Negro', 'A')

--Comprobamos registro
SELECT * FROM personal

--Insertamos 10 registros m�s
INSERT INTO personal VALUES
	('Jesus', 'Azul', '2022-05-01', 'A'), 
	('Javier', 'Azul', '2022-05-12', 'A'),
	('Arturo', 'Negro', GETDATE(), 'A'),
	('Guillermo', 'Rojo', '2022-05-16','A'),
	('Daniel', 'Verde', '2022-05-05','A')


--Actualizamos a estatus "B" (Baja) los de color Verde.
UPDATE personal
SET Estatus = 'B'
WHERE Color = 'Verde'

--Comprobamos por medio de un select
SELECT * FROM personal

--Crear tabla colores
CREATE TABLE colores(
	ID			INT,
	Color		VARCHAR(20)
)

--Insertamos los registros de colores en el cat�logo
INSERT INTO colores VALUES 
	(1,'Rojo'),
	(2,'Azul'),
	(3,'Verde'),
	(4,'Negro')

SELECT * FROM colores

--Actualizamos el campo color de la tabla personal para que muestre el ID y no la descripci�n del color
SELECT * FROM personal
SELECT * FROM colores

--Comprobamos por medio de un select
UPDATE personal
SET Color = c.ID
FROM 
	personal p
INNER JOIN
	colores c ON p.Color = c.Color

SELECT * FROM personal

--Por medio de un JOIN entre personal y colores mostrar en el siguiente orden:
--     personal.ID, personal.Nombre, personal.Color, colores.Color, personal.estatus

SELECT
	tabla1.campo, tabla2.campoN
FROM
	tabla1
INNER JOIN 
	tabla2 ON tabla1.campo = tabla2.campoN

--Agregar columna Salario de tipo DECIMAL(8,2), columna de comisi�n de tipo DECIMAL(8,2) y 
--TotalSueldo de tipo DECIMAL(8,2) 

DELETE from personal
where ID in (select ID from personal group by ID having count(ID) >1)

ALTER TABLE personal ADD Salario DECIMAL(8,2), Comision DECIMAL(8,2), TotalSueldo DECIMAL(8,2)
ALTER TABLE personal DELETE TotalSueldo 

--Hacer siguiente actualizaci�n por medio de una subconsulta en where
--      Rojo: 1500.50, Negro; 1900.00, Verde: 2506.85, Azul: 1000.00

UPDATE personal
SET Salario = 
(
	SELECT COLOR
	FROM colores
	WHERE ROJO = 1500.50 AND NEGRO = 1900.00 AND VERDE = 2506.85 AND AZUL = 1000
)

--Actualizar la comisi�n a 15% 

--Actualizar el sueldo total multiplicando sueldo * comisi�n

--Hacemos un Select para verificar datos
