--Ejercicio 4 operaciones basicas con tipo de dato decimal
--Crear una tabla con los campos Nombre, Apellido, Edad

DECLARE
@VAR1	DECIMAL(10,2),
@VAR2 DECIMAL(10,2)

SET @VAR1 = 100.25
SET @VAR2 = 50.75

SELECT @VAR1 + @VAR2
SELECT @VAR1 - @VAR2
SELECT @VAR1 * @VAR2
SELECT @VAR1 / @VAR2

CREATE TABLE PERSONA
(
	Nombre		char(10),
	Apellido	char(10),
	Edad		int
)