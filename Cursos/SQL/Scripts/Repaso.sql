/**** comandos con texto  **/
select *
from	Art
where	Articulo >= 'A' AND Articulo <='Z'
ORDER BY Categoria , Articulo DESC

select  Articulo, NombreCorto, Grupo, Categoria, Familia
from	Art
where	Articulo >= 'A' AND Articulo <='Z'
ORDER BY 4 , 1


SELECT Articulo, DESCRIPCION1, ISNULL(LEN(DESCRIPCION1),0), (ISNULL(DESCRIPCION1,'SIN DATOS')+'   -hOLA MUNDO   '), ISNULL(DESCRIPCION1,'SIN DATOS'),
		LTRIM(RTRIM((ISNULL('   '+DESCRIPCION1,'SIN DATOS')+'   -hOLA MUNDO   '))),
		SUBSTRING(LTRIM(RTRIM((ISNULL('   '+DESCRIPCION1,'SIN DATOS')+'   -hOLA MUNDO   '))),1,5),
		SUBSTRING(DESCRIPCION1,5,2)
FROM Art
WHERE LEN(DESCRIPCION1) > 1


/**** COMANDO DE NUMERICOS ***/
SUM()
COUNT(*) 

SELECT * FROM VENTA
WHERE ESTATUS ='CONCLUIDO'

SELECT Mov, COUNT(*)
FROM VENTA
WHERE ESTATUS ='CONCLUIDO'
GROUP BY Mov

SELECT COUNT(*)
FROM VENTA
WHERE ESTATUS ='CONCLUIDO'


SELECT EMPRESA, Mov, COUNT(*), SUM(IMPORTE)
FROM VENTA
WHERE ESTATUS ='CONCLUIDO'
GROUP BY Mov, Empresa
ORDER BY 1,2


SELECT EMPRESA, Mov,  SUM(IMPORTE)
FROM VENTA
WHERE ESTATUS ='CONCLUIDO'
GROUP BY Mov, Empresa
ORDER BY 1,2


SELECT   SUM(IMPORTE)
FROM VENTA
WHERE ESTATUS ='CONCLUIDO'

SET LANGUAGE 'SPANISH'

/***** COMANDOS DE FECHA)***/
SELECT * FROM VENTA

SELECT CONVERT(DATE,fECHAEMISION), fECHAeMISION, UltimoCambio,CONVERT(DATETIME,CONVERT(DATE,fECHAEMISION)) NUEVO,
		YEAR(fECHAeMISION),
		MONTH(fECHAeMISION),
		DAY(fECHAeMISION),
		DATENAME(MM,fECHAeMISION),
		DATENAME(DW,fECHAeMISION),
		DATENAME(DD,fECHAeMISION),
		DATEADD(D,2,fECHAeMISION),
		DATEADD(D,-2,fECHAeMISION),
		DATEADD(MM,-2,fECHAeMISION),
		DATEADD(YY,1,fECHAeMISION),
		DATEDIFF(DD,fECHAeMISION, UltimoCambio),
		DATEDIFF(MM,fECHAeMISION, UltimoCambio),
		DATEDIFF(YY,fECHAeMISION, UltimoCambio)
FROM Venta

create table xpcovproducto
(
	Producto			varchar(20),
	Descripcion			varchar(50)
)

create table xpcovsabores
(
	Sabor			varchar(20),
	descipcion		varchar(50),
	Producto		varchar(20)
)


create table xpcovalmacen
(
	Almacen			varchar(10),
	Producto		varchar(20)
)

insert into xpcovalmacen values ('Gral','Paleta')
insert into xpcovalmacen values ('Gral','Malteada')

insert into xpcovproducto values('Paleta','Paleta')
insert into xpcovproducto values('Helado','Helado')
insert into xpcovproducto values('Agua','Agua')
insert into xpcovproducto values('Congeladas','Congeladas')

select * from xpcovproducto


insert into xpcovsabores values('limón', 'limón','Paleta')
insert into xpcovsabores values('limón', 'limón','Helado')
insert into xpcovsabores values('limón', 'limón','Agua')
insert into xpcovsabores values('Fresa', 'Fresa','Paleta')
insert into xpcovsabores values('Fresa', 'Fresa','Malteada')



select * from xpcovsabores

select P.Producto, S.Sabor, S.Producto
from	xpcovproducto P inner join xpcovsabores S on P.Producto = S.Producto


select P.Producto, S.Sabor, S.Producto, A.Almacen
from	xpcovproducto P left join xpcovsabores S on P.Producto = S.Producto
		LEFT join xpcovalmacen A ON A.Producto = P.Producto


select P.Producto, S.Sabor, S.Producto
from	xpcovproducto P right join xpcovsabores S on P.Producto = S.Producto


select P.Producto, S.Sabor, S.Producto
from	xpcovproducto P  join  xpcovsabores S on P.Producto = S.Producto