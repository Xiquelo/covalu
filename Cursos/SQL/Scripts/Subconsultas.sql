/*SELECTS ANIDADOS o SubConsultas*/

SELECT * FROM Venta WHERE Ejercicio = 2021 AND Periodo = 2 AND Mov = 'Factura C.Renta'

--Tabla cat�logo continene registros �nicos en la tabla
SELECT * FROM Cte

--Tabla transaccional la cual contiene el registro de todas las transacciones realizadas
SELECT * FROM Venta

SELECT 
	ID, 
	Empresa,
	(SELECT Nombre FROM Empresa WHERE Empresa.Empresa = Venta.Empresa) as NombreEmpresa,
	Mov,
	FechaEmision
FROM 
	Venta
WHERE 
	Ejercicio = 2021
	AND Periodo = 2


-- Total de importe por empresa
SELECT 
	Empresa,
	SUM(Importe)
FROM 
	Venta v
WHERE 
	Ejercicio = 2021 AND Periodo IN (1,2,3)
GROUP BY
	Empresa

--Total de importe por empresa, pero agregando el nombre de la empresa.
SELECT 
	Empresa,
	(SELECT Nombre FROM Empresa e WHERE e.Empresa = v.Empresa) as NombreEmpresa,
	SUM(Importe)
FROM 
	Venta v
WHERE 
	Ejercicio = 2021 AND Periodo IN (1,2,3)
GROUP BY
	Empresa

--SubConsulta EN CONDICIONALES
SELECT 
	Empresa,
	(SELECT Nombre FROM Empresa e WHERE e.Empresa = v.Empresa) as NombreEmpresa,
	Sucursal,
	(SELECT Nombre FROM Sucursal s WHERE s.Sucursal = v.Sucursal),
	SUM(Importe)
FROM 
	Venta v
WHERE 
	Ejercicio = 2021 AND Periodo IN (1,2,3)
	AND Sucursal IN (SELECT Sucursal FROM Sucursal WHERE Sucursal LIKE '1001%')
GROUP BY
	Empresa, Sucursal

--Ejercicio AdventureWorks
SELECT 
	SalesOrderID,
	OrderDate, 
	SubTotal, 
	(SELECT SUM(OrderQty * UnitPrice) FROM Sales.SalesOrderDetail d WHERE d.SalesOrderID = h.SalesOrderID) as SubTotalDetalle
FROM 
	Sales.SalesOrderHeader h
WHERE 
	SalesOrderID = 43666

--Ejercicio filtrar por el campo de SalesOrderID de la tabla SalesOrderDetail por medio de subconsulta.
