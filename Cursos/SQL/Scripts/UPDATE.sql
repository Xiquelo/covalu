--CREAR BASE DE ATOS PARA EJERCICIOS
CREATE DATABASE TALLERSQL2022

--PONER EN USO LA BASE DE DATOS
USE TALLERSQL2022

--CREAR TABLA PARA PRIMER EJEMPLO
CREATE TABLE PERSONAL
(
	ID			INT IDENTITY(1,1),
	Nombre		VARCHAR(20),
	Color		VARCHAR(20),
	FechaAlta	DATETIME,
	Estatus		CHAR
)

--AÑADIR DEFAULT AL CAMPO FECHAALTA CON LA DEL SISTEMA
ALTER TABLE PERSONAL ADD DEFAULT GETDATE() FOR FECHAALTA 

INSERT INTO personal (Nombre, Color, Estatus) VALUES 
	('Juan', 'Verde', 'A'), ('Pedro', 'Negro', 'A')

INSERT INTO personal VALUES
	('Jesus', 'Azul', '2022-05-01', 'A'), 
	('Javier', 'Azul', '2022-05-12', 'A'),
	('Arturo', 'Negro', GETDATE(), 'A'),
	('Guillermo', 'Rojo', '2022-05-16','A'),
	('Daniel', 'Verde', '2022-05-05','A')

--COMPROBAMOS LOS REGISTROS
SELECT * FROM PERSONAL

--ACTUALIZAMOS A ESTATUS "B" (BAJA) LOS DE COLOR VERDE
UPDATE PERSONAL
SET ESTATUS = 'B'
WHERE COLOR = 'Verde'

--CREACION TABLA COLORES
CREATE TABLE colores
(
	ID			INT,
	Color		VARCHAR(20)
)

INSERT INTO colores VALUES 
	(1,'Rojo'),
	(2,'Azul'),
	(3,'Verde'),
	(4,'Negro')

--ACTUALIZAMOS EL CAMPO COLO DE LA TABLA PERSONAL PARA QUE MUESTRE EL ID U NO LA DESCRIPCION DEL COLOR 
UPDATE PERSONAL
SET P.COLOR = C.ID
FROM PERSONAL P
INNER JOIN COLORES C
ON (P.COLOR = C.COLOR)

--Por medio de un JOIN entre personal y colores mostrar en el siguiente orden:
--     personal.ID, personal.Nombre, personal.Color, colores.Color, personal.estatus
SELECT 
	P.ID,
	P.NOMBRE,
	P.COLOR,
	C.COLOR,
	P.ESTATUS
FROM 
	PERSONAL
INNER JOIN
	PERSONAL P ON P.COLOR = C.ID

--Agregar columna Salario de tipo DECIMAL(8,2), columna de comisión de tipo DECIMAL(8,2) y 
--TotalSueldo de tipo DECIMAL(8,2) 



--Hacer siguiente actualización por medio de una subconsulta en where
--      Rojo: 1500.50, Negro; 1900.00, Verde: 2506.85, Azul: 1000.00

--Actualizar la comisión a 15% 

--Actualizar el sueldo total multiplicando sueldo * comisión

--Hacemos un Select para verificar datos