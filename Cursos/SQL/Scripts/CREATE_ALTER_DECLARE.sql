CREATE DATABASE tallerSQL

--Comandos DDL Data Definition Language

--CREATE, ALTER, DROP

--CREATE Crear objetos dentro de la base de datos
--ALTER Permite la modificacion de la estructura de nuestros objetos
--DROP Va a borrar el objeto

USE tallerSQL


--Creacion de tabla para ejemplos de tipos de datos
CREATE TABLE tipoDato
(
	ID				INT,
	ejemploBIGINT	BIGINT
)

--Consulta de tabla
select * from tipoDato

INSERT INTO tipoDato VALUES(10.5, 9222337206854775800) --Tipo Int, Bigint

ALTER TABLE tipoDato ADD ejemploSmallInt SMALLINT --Creacion de campo con tipo de dato

INSERT INTO tipoDato VALUES(10.5, 9222337206854775800, 32767)  --Registro con valor smallint

--DECIMAL, NUMERIC 
ALTER TABLE tipoDato ADD ejemploDecimal DECIMAL(10,2)  --Creacion de campo DECIMAL
INSERT INTO tipoDato VALUES(10.5, 9222337206854775800, 32710, 12345678.9163)  --Registro con tipo de dato DECIMAL

ALTER TABLE tipoDato ADD caracter CHAR(1) --Creacion de campo Char
ALTER TABLE tipoDato ADD caracteres VARCHAR(10) --Creacion de campo Varchar

INSERT INTO tipoDato VALUES(10.5, 9222337206854775800, 32710, 12345678.9163)

ALTER TABLE tipoDato ADD fechaSimple DATE
ALTER TABLE tipoDato ADD fechaCompleta DATETIME

INSERT INTO tipoDato (fechasimple) VALUES ('2022-07-28')
INSERT INTO tipoDato (fechaCompleta) VALUES (GETDATE())


DECLARE
@Numero1		INT,
@Numero2		INT

SET @Numero1 = 10
SET @Numero2 = 20

SELECT @Numero1 + @Numero2

--Ejercicio 4 operaciones basicas con tipo de dato decimal
--Crear una tabla con los campos Nombre, Apellido, Edad

DECLARE
@VAR1	DECIMAL(10,2),
@VAR2 DECIMAL(10,2)

SET @VAR1 = 100.25
SET @VAR2 = 50.75

SELECT @VAR1 + @VAR2
SELECT @VAR1 - @VAR2
SELECT @VAR1 * @VAR2
SELECT @VAR1 / @VAR2

CREATE TABLE PERSONA
(
	Nombre		char(10),
	Apellido	char(10),
	Edad		int
)