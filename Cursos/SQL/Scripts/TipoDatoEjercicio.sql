--Comandos DDL Data Definition Language

--CREATE, ALTER, DROP

--CREATE Crear objetos dentro de nuestra base de datos

--ALTER Permite la modificaci�n de la estructura de nuestros objetos

--DROP Va a borrar el objeto

/*
CREATE DATABASE tallerSQL

USE tallerSQL

DROP DATABASE tallerSQL

USE Intelisis

DROP DATABASE tallerSQL
*/

CREATE DATABASE tallerSQL
USE tallerSQL

-- Creaci�n de tabla para ejemplos de tipos de datos
CREATE TABLE tipoDato(
	ID					INT,
	ejemploBIGINT		BIGINT
)

--Consulta de tabla
SELECT * FROM tipoDato

INSERT INTO tipoDato VALUES(10, 9223372036854775807)		-- Tipo INT, Tipo BigInt
INSERT INTO tipoDato VALUES(10.5, 9223372036854775800)

ALTER TABLE tipoDato ADD ejemploSmallInt SMALLINT			-- Creacci�n de campo con tipo de dato smallint

INSERT INTO tipoDato VALUES(15, 9223372036854775800, 32780) -- Registro con valor smallint

-- DECIMAL, NUMERIC

ALTER TABLE tipoDato ADD ejemploDecimal DECIMAL(10,2)		-- Creaci�n de campo DECIMAL 

INSERT INTO tipoDato VALUES(15, 9223372036854775800, 32710, 12345678.9163) -- Registro con tipo de dato DECIMAL

ALTER TABLE tipoDato ADD caracter CHAR(10)					-- Creacion de campo Char
ALTER TABLE tipoDato ADD caracteres varchar(10)				-- Creacion de campo varchar

INSERT INTO tipoDato (caracteres) VALUES('C')				-- Registro con tipo de dato char y varchar

ALTER TABLE tipoDato ADD fechaSimple DATE					-- Creaci�n de campo DATE
ALTER TABLE tipoDato ADD fechaCompleta DATETIME				-- Creaci�n de campo DATETIME

INSERT INTO tipoDato(fechasimple) VALUES('2022-07-28')
INSERT INTO tipoDato(fechaCompleta) VALUES(GETDATE())


--Ejemplo de suma de n�mero enteros
DECLARE
@Numero1		INT,
@Numero2		INT

SET @Numero1 = 10
SET @Numero2 = 20

SELECT @Numero1 + @Numero2

--Ejercicio 4 operaciones b�sicas con tipo de dato decimal.
--Crean una tabla con los campos Nombre, Apellido, Edad.

















