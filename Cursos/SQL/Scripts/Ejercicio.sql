/***********************************************************
Crear una tabla llamada Venta con los siguientes campos:
	ID,	FechaEmision, Estatus, Empresa, Importe, FechaAlta
	El campo ID, debe ser Identity
************************************************************/
CREATE TABLE Venta
(
	ID 				INT IDENTITY(1,1),
	FechaEmision	DATE,
	Estatus 		varchar(10),
	Empresa 		varchar(10),
	Importe 		INT,
	FechaAlta		DATE
)
/***********************************************************
Crear una tabla llamada VentaD con los siguientes campos:
	ID, Articulo, Cantidad, Precio
************************************************************/
CREATE TABLE VentaD
(
	ID 			INT,
	Articulo 	varchar(10),
	Cantidad 	int,
	Precio 		float
)
/***********************************************************
Crear una tabla llamada Empresa con los siguientes campos:
	Empresa, Nombre, Estatus, FechaAlta
************************************************************/
CREATE TABLE Empresa
(
	Empresa 	varchar(10),
	Nombre		varchar(10),
	Estatus 	varchar(10),
	FechaAlta 	DATE
)
/***********************************************************
Crear una tabla llamada Articulo con los siguientes campos:
	Articulo, Nombre, Categoria, Estatus, FechaAlta
************************************************************/
CREATE TABLE Articulo
(
	Articulo 	varchar(10),
	Nombre 		varchar(10),
	Categoria 	varchar(10),
	Estatus 	varchar(10),
	FechaAlta 	DATE
)
/***********************************************************
Agregar default 'ALTA' en el campo estatus de la tabla empresa
	y la tabla Articulo 
************************************************************/
ALTER TABLE Empresa ADD DEFAULT 'ALTA' FOR Estatus
ALTER TABLE Articulo ADD DEFAULT 'ALTA' FOR Estatus
/***********************************************************
Agregar default con fecha del sistema en los campos FechaAlta
	de las tablas
************************************************************/
ALTER TABLE Venta ADD DEFAULT GETDATE() FOR FechaAlta
ALTER TABLE Empresa ADD DEFAULT GETDATE() FOR FechaAlta
ALTER TABLE Articulo ADD DEFAULT GETDATE() FOR FechaAlta
/***********************************************************
Insertar registros en las tablas 
************************************************************/
--Venta
INSERT INTO 
	Venta (FechaEmision, Estatus, Empresa)
Values
	('2021-05-31', 'CONCLUIDO', 'C001'),
	('2022-05-31', 'CONCLUIDO', 'C002'),
	('2022-05-31', 'CONCLUIDO', 'C003'),
	('2022-01-12', 'CONCLUIDO', 'C004'),
	('2021-08-23', 'CONCLUIDO', 'C005'),
	('2021-04-15', 'CONCLUIDO', 'C002'),
	('2022-03-20', 'CONCLUIDO', 'C004'),
	('2022-02-11', 'CONCLUIDO', 'C001'),
	('2022-01-24', 'CONCLUIDO', 'C003'),
	('2022-05-20', 'CONCLUIDO', 'C002'),
	('2022-05-21', 'CONCLUIDO', 'C002'),
	('2022-05-22', 'CONCLUIDO', 'C004'),
	('2021-12-31', 'CONCLUIDO', 'C005'),
	('2021-11-10', 'CONCLUIDO', 'C001'),
	('2021-12-20', 'CONCLUIDO', 'C001')

--VentaD
INSERT INTO 
	VentaD (ID, Articulo, Cantidad, Precio)
VALUES
	(1, 'ART0001', 5, 10.00),
	(1, 'ART0002', 6, 24.00),
	(2, 'ART0003', 9, 16.00),
	(3, 'ART0004', 7, 45.00),
	(3, 'ART0005', 8, 12.00),
	(3, 'ART0006', 9, 10.00),
	(4, 'ART0007', 2, 3.00),
	(4, 'ART0001', 2, 10.00),
	(4, 'ART0008', 1, 14.00),
	(5, 'ART0001', 3, 10.00),
	(5, 'ART0005', 6, 12.00),
	(5, 'ART0009', 5, 100.00),
	(5, 'ART0010', 8, 5.00),
	(5, 'ART0001', 10, 10.00),
	(6, 'ART0001', -1, 10.00),
	(7, 'ART0002', 2, 24.00),
	(7, 'ART0005', 1, 12.00),
	(8, 'ART0009', 4, 100.00),
	(8, 'ART0010', 3, 5.00),
	(8, 'ART0006', 11, 10.00),
	(9, 'ART0007', 3, 3.00),
	(9, 'ART0008', 5, 14.00),
	(10, 'ART0001', 10, 10.00),
	(10, 'ART0002', 18, 24.00),
	(10, 'ART0003', -20, 16.00),
	(11, 'ART0004', 2, 45.00),
	(12, 'ART0004', 4, 45.00),
	(13, 'ART0002', 3, 24.00),
	(13, 'ART0001', 10, 10.00),
	(14, 'ART0010', 20, 5.00),
	(14, 'ART0009', 4, 100.00),
	(14, 'ART0008', 3, 14.00),
	(14, 'ART0001', 1, 10.00),
	(15, 'ART0010', -20, 5.00)

--Empresa
INSERT INTO Empresa
	(Empresa, Nombre, Estatus)
VALUES
	('C001', 'Empresa1', 'ALTA'),
	('C002', 'Empresa2', 'ALTA'),
	('C003', 'Empresa3', 'BAJA'),
	('C004', 'Empresa4', 'ALTA'),
	('C005', 'Empresa5', 'BAJA')

--Articulo
INSERT INTO Articulo
	(Articulo, Nombre, Categoria, Estatus)
VALUES
	('ART0001', 'Papas', 'Abarrotes', 'ALTA'),
	('ART0002', 'Aceite', 'Abarrotes', 'ALTA'),
	('ART0003', 'Refresco', 'Abarrotes', 'BAJA'),
	('ART0004', 'Jabon', 'Abarrotes', 'ALTA'),
	('ART0005', 'Galletas', 'Abarrotes', 'ALTA'),
	('ART0006', 'Agua', 'Abarrotes', 'ALTA'),
	('ART0007', 'Dulce', 'Abarrotes', 'ALTA'),
	('ART0008', 'Jugo', 'Abarrotes', 'ALTA'),
	('ART0009', 'Formula', 'Abarrotes', 'BAJA'),
	('ART00010', 'Paleta', 'Abarrotes', 'ALTA')

/***********************************************************
Actualizar el estatus a 'PENDIENTE' a las ventas que se han 
	generado en el mes corriente
************************************************************/
UPDATE 	Venta
SET 	Estatus = 'PENDIENTE'
WHERE 	FechaEmision LIKE '%-05-%' 
/***********************************************************
Actualizar el importe de venta por medio de la suma de
	la cantidad por el precio del detalle de venta
************************************************************/
UPDATE 	Venta
SET 	Importe = 
/***********************************************************
Actualizar a estatus 'CANCELADO' las ventas con importe
	negativo
************************************************************/

/***********************************************************
Actualizar a estatus 'PENDIENTE' las ventas donde el 
	articulo tenga estatus pendiente
************************************************************/
UPDATE 		Venta
SET 		V.Estatus = A.Estatus
FROM 		Venta v
INNER JOIN	Articulo a
ON 			(A.ID = VD.ID)
INNER JOIN	VentaD  VD
ON 			(VD.ID = V.ID)
/***********************************************************
Eliminar los registros con fecha anterior del 1 de Enero
	del 2022 y donde los Art�culos est�n con estatus
	'BAJA'
************************************************************/
DELETE FROM Venta
WHERE FechaEmision IN 
	(
		SELECT 
			V.FechaEmision, VD.Estatus 
		FROM 
			Venta V
		INNER JOIN 
			VentaD VD 
		ON
			VD.ID = V.ID
		INNER JOIN
			Articulo A
		ON
			A.Articulo = VD.Articulo
		WHERE V.FechaEmision < 2022-01-01
		AND VD.Estatus = 'BAJA'
	)
/***********************************************************
Muestra el importe Total agrupado por Nombre de empresa
************************************************************/

/***********************************************************
Muestra el importe Total agrupado por Nombre de Articulo
************************************************************/

/***********************************************************
En caso de equivocarse en la insercci�n de registros usar 
	sentencia TRUNCATE para no afectar el autoincrementable
	en la tabla Venta
************************************************************/