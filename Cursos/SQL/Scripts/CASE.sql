/*CASE WHEN CONDICION THEN RESULTADO

WHEN CONDICION THEN RESULTADO
WHEN CONDICION THEN RESULTADO
WHEN CONDICION THEN RESULTADO
WHEN CONDICION THEN RESULTADO
WHEN CONDICION THEN RESULTADO

ELSE RESULTADO END*/

SELECT DISTINCT TIPO FROM PERSONAL 

SELECT PERSONAL, TIPO, NOMBRE 
(
	CASE 	
		WHEN TIPO = 'Asimilados' then PERSONAL+'-'+
				SUBSTRING(PERSONAL,1,1)
		WHEN TIPO = 'Becario' then PERSONAL
		WHEN TIPO = 'Empleado' then SUBSTRING(PERSONAL,1,1)+'-'+PERSONAL
		ELSE ''
	END
) calculo
FROM PERSONAL

--validar del estado que viene en ese campo 

SELECT ESTADO,
(
	CASE 
		WHEN SUBSTRING(ESTADO,1,1) IN ('A','B','C','D','E','F','G','H','I') THEN 1
		WHEN SUBSTRING(ESTADO,1,1) IN ('J','K','L','M','N','O','P','Q','R') THEN 2
		ELSE 3
	END
) TIPO
FROM PERSONAL