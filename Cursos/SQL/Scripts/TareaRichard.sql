SET LANGUAGE 'SPANISH'

--REPORTE DE LAS VENTAS POR MES DEL AÑO 2021 IMPORTE Y CANTIDAD DE REGISTROS

SELECT DATENAME(MONTH,FechaEmision), SUM(IMPORTE), COUNT(*)
FROM VENTA
WHERE YEAR(FECHAEMISION) = 2020
GROUP BY DATENAME(MONTH,FECHAEMISION)
ORDER BY 1

SELECT DATENAME(MONTH,FechaEmision), CLIENTE, MOV, SUM(IMPORTE), COUNT(*)
FROM VENTA
WHERE YEAR(FECHAEMISION) = 2020
GROUP BY DATENAME(MONTH,FECHAEMISION)
ORDER BY 1

SELECT datename(MONTH,FechaEmision), sum(importe)
from venta
where year(FechaEmision) = 2021
group by datename(MONTH,FechaEmision)
order by 1


--REPORTE DE TODOS LOS MOVIMIENTOS CANCELADOS POR CLIENTE

SELECT CLIENTE, SUM(IMPORTE), COUNT(*)
FROM VENTA
WHERE ESTATUS = 'CANCELADO'
GROUP BY CLIENTE

--EL PROMEDIO DE VENTA POR CLIENTE DEL AÑO 2021
SELECT CLIENTE, AVG(IMPORTE), COUNT(*)
FROM VENTA
WHERE FECHAEMISION LIKE '%2021%'
GROUP BY CLIENTE

--REPORTE POR TIPO DE MOVIMIENTO Y POR CLIENTE DEL AÑO 2022 IMPORTE Y CANTIDAD DE REGISTROS

SELECT CLIENTE, MOV, SUM(IMPORTE), COUNT(*)
FROM VENTA
WHERE FECHAEMISION LIKE '%2021%'
GROUP BY CLIENTE, MOV

--CUANTOS REGISTROS HAY POR MOVIMIENTO

SELECT MOV, COUNT(*)
FROM VENTA
GROUP BY MOV

--STATUS POR CLIENTE Y MOVIMIENTO, CANTIDAD DE REGISTROS

SELECT ESTATUS, CLIENTE, MOV, COUNT(*)
FROM VENTA
GROUP BY Estatus, CLIENTE, MOV

--VENTAS POR CLIENTE TOTALES, ESTATUS COCLUIDO SUMA, DIFERENTE ESTATUS RESTA

SELECT 		CLIENTE, ESTATUS, (IMPORTE), 
FROM 		VENTA
WHERE 		ESTATUS = 'CONCLUIDO'
GROUP BY 	Estatus, CLIENTE

--CUANTAS VENTAS HAY POR HORA AÑO MES DIA
SELECT 
	DATEPART(HOUR, FECHAEMISION),
	COUNT(IMPORTE)
FROM 
	VENTA
WHERE 
	YEAR(FECHAEMISION) = 2020 
	AND MONTH(FECHAEMISION) = 1 
	AND DAY(FECHAEMISION) = 17
GROUP BY DATEPART(HOUR, FECHAEMISION)
