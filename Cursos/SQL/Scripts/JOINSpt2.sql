SELECT
	*
FROM 
	Venta

SELECT
	*
FROM 
	Empleado

--Ejemplo RIGHT JOIN

USE Tolukasa
SELECT
	*
FROM 
	VentaD
RIGHT JOIN
	Empleado e ON v.Empleado = e.ID

SELECT
	ID,
	FechaEmision,
	Importe,
	v.Cliente,
	Nombre
FROM 
	Venta v
FULL OUTER JOIN
	Cte e ON v.Cliente = e.Cliente
WHERE
	FechaEmision >= '2021-01-01'

/*Mostrar Empresa, Cuenta, Cargos y Abonos de la tabla Acum, el campo descripcion y tipo de la tabla Cta 
Del Ejercicio 2019 y Periodo 12 y que el tipo de Cuenta sea MAYOR*/







