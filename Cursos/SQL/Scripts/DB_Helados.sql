create database helados

use helados

create table sabores 
(
	Sabor			varchar (50),
	Tipo			varchar (10),
	Tamano			varchar (30),
	Complementos	varchar (30),
	Costo			int
)

select * from sabores

insert into sabores values	('fresa', 'leche', 'mediano', 'cubierta chocolate', 20),
							('vainilla', 'leche', 'grande', 'chispas chocolate', 30)