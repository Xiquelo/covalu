# Notas SQL 

DB: Repositorio de datos relacionados y con sentido

Oracle: Crea pequenos archivos de almacenamiento

Sql server: Crea un unico archivo por DB y un archivo LOG


## Objetos de SQL

Tablas: espacios físicos dentro de la DB y que almacenan datos

Vistas: Conjunto de tablas dentro de una sola.
No se pueden modificar, solo se pueden visualizar. 

Funcion: Es un programa pequeno que recibe una variable y siempre devuelve un resultado. Siempre recibe una variable.

Procedimiento: Un programa más grande que una función y que puede devolver más de un resultado

Secuencias: una serie de valores consecutivos

Trigger: Programa siempre activo que reacciona a un estimulo de la DB
    Eliminacion de registros/actualizacion de registros o insercion de registros

Paquete (Oracle)

Usuarios: Propietarios de Objetos (Esquemas) / Uso de los objetos de otros usuarios

Indice: Forma de ordenar los datos por un atributo que pueda tener un dato. Crea un espacio aislado donde mantiene los datos ordenados y siempre tiene que andarse regenerando

Reporte 


## Lenguajes de manipulacion

Data Definition Language (DDL): Manipular la informacion
    Create / Drop / Alter

Data Manipulation Language (DML): Manipular los datos


Select
Buscar informacion en tablas o vistas

Update
Delete
Insert

## DB de sistema

Master: Aloja todos los metadatos de la base de datos
Administra la instalacion de SQL server	

Model: copia de la base datos model para hacer una DB nueva
Cada nueva DB copia la estructura de MODEL

MSDB: Replicaciones, lleva la configuracion de las replicas entre DB
Lleva el proceso de respaldo de las DB

TempDB: llevara todos los archivos log de la DB

Loggins: muestra los usuarios que tienen acceso a la DB


## Funciones de agregado

- AVG: Devuelve el promedio de los valores 

- MIN: Devuelve el valor minimo 

- SUM: Suma los valores 

- COUNT: Cuenta la cantidad de celdas 

- STDEV: Devuelve la desviacion estandar Muestral 

- STDEVP: Devuelve la desviacion estandar Poblacional 

- VAR: Devuelve la varizza muestral 

- VARP: Devuelve la varianza poblacional 

- MAX: Devuelve el valor maximo 


## Funciones de Fecha y Hora

- SYSDATETIME: Fecha y Hora del sistema

- GETDATE: Obtiene la fecha del servidor

- DATENAME: Nombre de una parte de la fecha determinada

- __DAY: El numero del dia del mes

- MONTH: El numero de mes

- YEAR: El año de una fecha

- DATEDIFF: La diferencia entre dos fechas

- DATEADD: Añade partes de una fecha a una fecha determinada

- DATEPART: Parte de una fecha

### Partes de fecha
year 		yy,yyyy
quarter		qq,q
month		mm,m
dayofyear	dy,y
day		dd,d
week		wk,ww
weekday		dw
hour 		hh
minute		mi,n
second		ss,s


Funciones de Cadena
- CONCAT: Concatena cadenas de caracteres 

    SELECT CONCAT( 'HOLA', 'MUNDO' ); 

= HOLAMUNDO

- ASCII: Retorna el valor ASCII del primer caracter

    SELECT ASCII( 'HOLA MUNDO' );

= 104

- LTRIM: elimina espacios en blanco al inicio de la cadena

    SELECT LTRIM( '     HOLA MUNDO     ' );

= HOLA MUNDO

- RTRIM: elimina los espacios en blanco del final de la cadena

    SELECT RTRIM( '     HOLA MUNDO     ' );

=      HOLA MUNDO

- SPACE: devuelve una cadena de espacios

    SELECT ( 'HOLA' + SPACE( 5 ) + 'MUNDO' );

= HOLA     MUNDO

- STR: convierte un numero a tipo caracter

    SELECT STR( 100 );

= 100

- STUFF: Inserta una cadena dentro de otra

    SELECT STUFF( 'HOLA',1,2,'MUNDO');

= MUNDOLA

    SELECT STUFF( 'HOLA',1,4,'MUNDO');

= MUNDO

- REPLACE: Reemplaza una cadena con otra

    SELECT REPLACE( 'HOLA','HOLA','MUNDO' );

= MUNDO

    SELECT REPLACE( ( 'HOLA' + SPACE( 5 ) + 'MUNDO' ), ' ', '' );

= HOLAMUNDO

- LEFT/RIGHT: Obtiene caracteres de la izquierda/derecha de una cadena

    SELECT LEFT( 'HOLA MUNDO', 5 );

= HOLA 

    SELECT RIGHT( 'HOLA MUNDO', 5 );

= MUNDO

- SUBSTRING: Extrae una cantidad de caracteres de una cadena

    SELECT SUBSTRING( 'HOLA MUNDO', 3, 5 );

= LA MU

- REPLICATE: Repite una cadena de caracteres

    SELECT REPLICATE( 'HOLA MUNDO', 3 );

= HOLA MUNDOHOLA MUNDO HOLA MUNDO

- REVERSE: Invierte la escritura de una cadena

    SELECT REVERSE( 'HOLA MUNDO' );

= ODNUM ALOH

- LOWER/UPPER: Convierte a minusculas/mayusculas

    SELECT LOWER( 'HOLA MUNDO' );

= hola mundo

    SELECT UPPER( 'hola mundo' );

= HOLA MUNDO

- LEN: Obtiene la longitud de cadena

    SELECT LEN( 'HOLA MUNDO' );

= 10


## CURSORES
Los cursores permiten almacenar los datos de una consulta T-SQL en memoria y poder manipular los datos de los elementos resultantes para realizar operaciones con ellos. Debe de usarse con cuidado ya que consume mucha memoria. La instruccion select debe ser lo mas selectiva posible 

Proceso para declarar, abrir, usar, cerrar y liberar los datos de un cursor.
1. Declarar el cursor, utilizando DECLARE Cursor
    DECLARE NOMBRECURSOR CURSOR FOR INSTRUCCION SELECT
2. Abrir el cursor, utilizando OPEN
    OPEN NOMBREDELCURSOR
3. Leer los datos del cursor, utilizando FETCH ... INTO
    FETCH NOMBREDELCURSOR
Si se va a recorrer el cursor, se debe almacenar los datos de cada registro en variables previamente definidas con la variante siguiente
    FETCH NOMBREDEL CURSOR INTO LISTAVARIABLES
4. Cerrar el cursor, utilizando CLOSE
    CLOSE NOMBREDELCURSOR
5. Liberar el cursor, utilizando DEALLOCATE
    DEALLOCATE NOMBREDELCURSOR

### Ejercicio 1
**Cursor que reporta los Empleados**
    DECLARE CURSOREMPLEADOS CURSOR FOR
    SELECT EMPLOYEEID, LASTNAME, FIRSTNAME FROM EMPLOYEES
**Abrir el cursor**
    OPEN CURSOR EMPLEADOS
**Los datos disponibles se visualizan con Fetch, ejecutar varias veces para ver los resultados**
    FETCH CURSOREMPLEADOS
La funcion @@FETCH_STATUS reporta CERO si hay registro
reporta -1 si ya no existen registro para mostrar
    select @@FETCH_STATUS
cerrar el cursor, liberar de memoria
    CLOSE CURSOREMPLEADOS
    DEALLOCATE CURSOREMPLEADOS

### Ejercicio 2
**Cursor que reporte la lista de productos, si las unidades en orden son mayores al stock, mostrar COMPRAR URGENTE, de lo contrario mostrar STOCK ADECUADO**
**Nota: El ejercicio muestra mensajes, los que en la practica  no son realmente utiles.

    DECLARE CURSORPRODUCTOCOMPRASURGENTE CURSOR FOR 
    SELECT P.PRODUCTID, P.PRODUCTNAME,
    P.UNITSINSTOCK, P.UNITSONORDER
    FROM PRODUCTS AS P

    OPEN CURSORPRODUCTOCOMPRASURGENTE

    DECLARE @CODIGO INT, @DESCRIPCION NVARCHAR( 40 ),
    @STOCK NUMERIC( 9,2 ), @PORATENDER NUMERIC( 9,2 )

    FETCH CURSORPRODUCTOCOMPRASURGENTE INTO @CODIGO,
    @DESCRIPCION, @STOCK, @PORATENDER
    PRINT '================ LISTADO ======================'
    WHILE ( @@FETCH_STATUS=0 )
        BEGIN
        DECLARE @MENSAJE NVARCHAR( 20 )
        IF ( @PORATENDER > @STOCK )
            BEGIN
            SET @MENSAJE = 'COMPRAR URGENTE'
            END
            ELSE
                BEGIN
                SET @MENSAJE = 'STOCK ADECUADO'
                END

    PRINT 'CODIGO: ' + STR( @CODIGO )
    PRINT 'DESCRIPCION: ' + @DESCRIPCION + 'STOCK: ' + 
    LTRIM( STR( @STOCK ) ) + ' POR ATENDER: ' + 
    LTRIM( STR( @PORATENDER ) )
    PRINT 'MENSAJE: ' + @MENSAJE
    PRINT '————————————————————————————————---------------'

    FETCH CURSORPRODUCTOCOMPRASURGENTE INTO @CODIGO,
    @DESCRIPCION, @STOCK, @PORATENDER
    END

    CLOSE CURSORPRODUCTOCOMPRASURGENTE
    DEALLOCATE CURSORPRODUCTOCOMPRASURGENTE

## IF-ELSE
Impone condiciones de ejecucion a una instruccion SQL. La instruccion SQL seguida de la palabra clave IF se ejecuta si la condicion se cumple: la expresion booleana devuelve TRUE. La palabra clave opcional ELSE introduce otra instruccion que se ejecuta cuano la condicion IF no se cumple: la expresion booleana devuelve FALSE. 

### Syntax
IF condicion
instruccion sql
ELSE
instruccion sql

### Ejemplo
Obtener el dia actual de la semana y decir si se trata de un dia laboral o no.
    IF DATENAME(weekday, GETDATE()) IN (N'Saturday', N'Sunday')
       SELECT 'Weekend';
    ELSE 
       SELECT 'Weekday';


## WHILE
Establece una condicion para la ejecucion repetida de una instruccion o bloque de instrucciones SQL. Las instrucciones se ejecutan repetidamente siempre que la condicion especificada sea verdadera. Se puee controlar la ejecucion de instrucciones en el bucle con las palabras clave BREAK y CONTINUE.

### Syntax
WHILE condicion
instruccion sql | BREAK | CONTINUE

### Ejemplo
    USE AdventureWorks2017;  
    GO  
    WHILE (SELECT AVG(ListPrice) FROM Production.Product) < $300  
    BEGIN  
       UPDATE Production.Product  
          SET ListPrice = ListPrice * 2  
       SELECT MAX(ListPrice) FROM Production.Product  
       IF (SELECT MAX(ListPrice) FROM Production.Product) > $500  
          BREAK  
       ELSE  
          CONTINUE  
    END  
    PRINT 'Too much for the market to bear';
