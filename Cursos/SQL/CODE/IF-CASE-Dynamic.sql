/*LA VENTA DE VENTA, OBTENER LOS DIFERENTES MOVIMIENTOS Y SUS ESTATUS

CUANTOS REGISTROS HAY POR TIPO DE MOVIMIENTO
Y CUANDO EL MOVIMIENTO SEA CONCLUIDO VA SUMAR, CUANDO SEA CANCELADO VA RESTAR AL MONTO DE VENTA, LO MISMO VA A PASAR CON LA CANTIDAD DE PIEZAS*/

DECLARE
    @MOV                VARCHAR(50),
    @MONTO              DECIMAL(26,2),
    @CANTIDAD           DECIMAL(26,2),
    @ESTATUS            VARCHAR(15),
    -----------------------------------
    @VENTATOTAL         DECIMAL(26,2),
    @CANTIDADTOTAL      DECIMAL(26,2),
    @VENTATOTALC        DECIMAL(26,2),
    @CANTIDADTOTALC     DECIMAL(26,2),
    -----------------------------------
    @SQL                VARCHAR(400),
    @SQL1               VARCHAR(400),
    @SQL2               VARCHAR(400),
    @SQL3               VARCHAR(400)

    DROP TABLE #XPCOVMOVIMIENTOSTIPO
    CREATE TABLE #XPCOVMOVIMIENTOSTIPO
        (
            MOVIMIENTO  VARCHAR(50),
            CANTIDAD    INT
        )

    SET @VENTATOTAL     = 0
    SET @CANTIDADTOTAL  = 0
    SET @VENTATOTALC    = 0
    SET @CANTIDADTOTALC = 0
    SET @SQL            = 0
    SET @SQL1           = 0
    SET @SQL2           = 0
    SET @SQL3           = 0

    DECLARE CRVENTAS CURSOR LOCAL FOR
        SELECT    
            TOP 70 
            V.MOV, 
            V.ESTATUS, 
            SUM(VD.CANTIDAD), 
            SUM(VD.PRECIO)    
        FROM VENTA V 
        INNER JOIN 
            VENTAD VD ON V.ID = VD.ID
        GROUP BY 
            V.MOV, V.ESTATUS

    OPEN CRVENTAS
    FETCH NEXT 
    FROM CRVENTAS 
    INTO 
        @MOV, 
        @ESTATUS, 
        @CANTIDAD, 
        @MONTO

    WHILE @@FETCH_STATUS    =    0
        BEGIN
            SET @SQL = ''
            ---- VALIDAR QUE EL TIPO DE MOVIMIENTO YA EXISTA
            IF EXISTS
                (
                    SELECT 1 
                    FROM #XPCOVMOVIMIENTOSTIPO 
                    WHERE MOVIMIENTO = @MOV
                )
                BEGIN
                    SET @SQL = '
                        UPDATE #XPCOVMOVIMIENTOSTIPO 
                        SET CANTIDAD = (CANTIDAD + 1) 
                        WHERE MOVIMIENTO = '
                            +''''+LTRIM(RTRIM(@MOV))+''''
                END
            ELSE
                BEGIN
                    SET @SQL ='
                        INSERT INTO #XPCOVMOVIMIENTOSTIPO 
                            (
                                MOVIMIENTO, 
                                CANTIDAD
                            ) 
                        VALUES( '+''''+LTRIM(RTRIM(@MOV))+''''+', 1)'
                END

            EXEC(@SQL)
            ---SELECT @SQL

			--SET @SQL1 = ''
            IF @ESTATUS = 'CONCLUIDO'
                BEGIN
                    SET @SQL1 = 
                        'SET @VENTATOTAL     = (' + 
                            CONVERT( VARCHAR, @VENTATOTAL ) + '+ (' +
                                CONVERT( VARCHAR, @CANTIDAD ) + '*' +
                                CONVERT( VARCHAR, @MONTO ) + '))'
                    -- SET @VENTATOTAL     = (@VENTATOTAL + (@CANTIDAD * @MONTO))
                    SET @SQL1 = 
                        'SET @CANTIDADTOTAL     = (' + 
                            CONVERT( VARCHAR, @CANTIDADTOTAL ) + '+ (' +
                                CONVERT( VARCHAR, @CANTIDAD ) + '))'
                    -- SET @CANTIDADTOTAL  = (@CANTIDADTOTAL + (@CANTIDAD))
                END
            ELSE
                BEGIN
                    SET @SQL1 = 
                        'SET @VENTATOTAL     = (' + 
                            CONVERT( VARCHAR, @VENTATOTAL ) + '- (' +
                                CONVERT( VARCHAR, @CANTIDAD ) + '*' +
                                CONVERT( VARCHAR, @MONTO ) + '))'
                    -- SET @VENTATOTAL     = (@VENTATOTAL - (@CANTIDAD * @MONTO))
                    SET @SQL1 = 
                        'SET @CANTIDADTOTAL     = (' + 
                            CONVERT( VARCHAR, @CANTIDADTOTAL ) + '- (' +
                                CONVERT( VARCHAR, @CANTIDAD ) + '))'
                    -- SET @CANTIDADTOTAL  = (@CANTIDADTOTAL - (@CANTIDAD))
                END 

            EXEC(@SQL1)

            SET @SQL2 =
            'SET @VENTATOTALC = (' +
            CONVERT( VARCHAR, @VENTATOTALC ) +
            '+ ( CASE WHEN @ESTATUS = ''CONCLUIDO'' THEN((' +
                    CONVERT( VARCHAR, @CANTIDAD ) +
                    '*' +
                    CONVERT( VARCHAR, @MONTO ) +
                    '))' +
            'ELSE (((' + CONVERT( VARCHAR, @CANTIDAD ) +
                        '*' +
                        CONVERT( VARCHAR, @MONTO ) +
                        ')) * -1) END ))'
            EXEC(@SQL2)

            /*
            SET @VENTATOTALC = 
                ( @VENTATOTALC + 
                    (
                        CASE 
                            WHEN @ESTATUS = 'CONCLUIDO' 
                                THEN ( ( @CANTIDAD * @MONTO ) ) 
                            ELSE ( ( ( @CANTIDAD * @MONTO ) ) * -1 ) 
                        END
                    )
                )
            */
            SET @SQL3 =
            'SET @CANTIDADTOTALC = (' +
            CONVERT( VARCHAR, @CANTIDADTOTALC ) +
            '+ ( CASE WHEN @ESTATUS = ''CONCLUIDO'' THEN' +
                    CONVERT( VARCHAR, @CANTIDAD ) +
            'ELSE (' + CONVERT( VARCHAR, @CANTIDAD ) +
                        ' * -1) END )'
            EXEC(@SQL3)

            /*
            SET @CANTIDADTOTALC = 
                ( @CANTIDADTOTALC + 
                    (
                        CASE 
                            WHEN @ESTATUS = 'CONCLUIDO' 
                                THEN @CANTIDAD 
                            ELSE ( @CANTIDAD *-1 ) 
                        END
                    )
                )
            */

            FETCH NEXT FROM CRVENTAS 
            INTO 
                @MOV, 
                @ESTATUS, 
                @CANTIDAD, 
                @MONTO
        END

    CLOSE CRVENTAS
    DEALLOCATE CRVENTAS

    SELECT @VENTATOTAL
    SELECT @VENTATOTALC
    SELECT @CANTIDADTOTAL
    SELECT @CANTIDADTOTALC

    SELECT * FROM #XPCOVMOVIMIENTOSTIPO
