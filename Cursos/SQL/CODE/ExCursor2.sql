DECLARE --DECLARACION DE VARIABLES
    @ALTAHOMBRE INT,
    @BAJAHOMBRE INT,
    @PERSONAL   VARCHAR( 15 ),
    @ESTATUS    VARCHAR( 15 ),
    @SEXO       VARCHAR( 15 ),
    @ALTAMUJER  INT,
    @BAJAMUJER  INT

SET @ALTAHOMBRE = 0
SET @BAJAHOMBRE = 0
SET @ALTAMUJER = 0
SET @BAJAMUJER = 0

DECLARE C_ALTABAJA CURSOR LOCAL FOR
    SELECT PERSONAL, ESTATUS, SEXO FROM PERSONAL

OPEN C_ALTABAJA
FETCH NEXT FROM C_ALTABAJA INTO @PERSONAL, @ESTATUS, @SEXO

WHILE @@FETCH_STATUS = 0
    BEGIN
        IF @ESTATUS = 'ALTA' AND @SEXO = 'FEMENINO' 
            SET @ALTAMUJER = ( @ALTAMUJER + 1 )
        ELSE IF @ESTATUS = 'ALTA' AND @SEXO = 'MASCULINO' 
            SET @ALTAHOMBRE = ( @ALTAHOMBRE + 1 )

        IF @ESTATUS = 'BAJA' AND @SEXO = 'FEMENINO'
            SET @BAJAMUJER = ( @BAJAMUJER + 1 )
        ELSE IF @ESTATUS = 'BAJA' AND @SEXO = 'MASCULINO'
            SET @BAJAHOMBRE = ( @BAJAHOMBRE + 1 )

        FETCH NEXT FROM C_ALTABAJA INTO @PERSONAL, @ESTATUS, @SEXO
    END

CLOSE C_ALTABAJA
DEALLOCATE C_ALTABAJA

SELECT 'PERSONAL CON ESTATUS ALTA CON SEXO FEMENINO: ' + CONVERT( VARCHAR, @ALTAMUJER )
SELECT 'PERSONAL CON ESTATUS ALTA CON SEXO MASCULINO: ' + CONVERT( VARCHAR, @ALTAHOMBRE )
SELECT 'PERSONAL CON ESTATUS BAJA CON SEXO FEMENINO: ' + CONVERT( VARCHAR, @BAJAMUJER )
SELECT 'PERSONAL CON ESTATUS BAJA CON SEXO MASCULINO: ' + CONVERT( VARCHAR, @BAJAHOMBRE )

