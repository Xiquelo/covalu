/*Diario de SQL*/

/*Creacion DB*/
CREATE DATABASE PLAYGROUND


/*Moverse entre DB*/
USE PLAYGROUND


/*Creacion de Tablas*/
CREATE TABLE PRUEBA 
(
    ID 		INT IDENTITY(1,1),
    OBJ		VARCHAR(10),
    OBJ2	VARCHAR(10),
    FECHA	TIMESTAMP
);

--Crear tablas con SELECT INTO--
--TEMPORAL--
USE TEMPDB; --se utilizara una DB temporal
GO 

IF OBJECT_ID (N'#BICYCLES',N'U') IS NOT NULL
DROP TABLE #BICYCLES;
GO

SELECT *
INTO 	#BICYCLES
FROM 	ADVENTUREWORKS2017.PRODUCTION.PRODUCT
WHERE 	PRODUCTNUMBER LIKE 'BK%';
GO

--PERMANENTE--
USE ADVENTUREWORKS2017; --utilizamos la DB de AdventureWorks
GO

IF OBJECT_ID('DBO.NEWPRODUCTS', 'U') IS NOT NULL
    DROP TABLE DBO.NEWPRODUCTS;
GO

ALTER DATABASE ADVENTUREWORKS2017 
SET RECOVERY BULK_LOGGED;
GO

SELECT * 
INTO 	DBO.NEWPRODUCTS
FROM 	PRODUCTION.PRODUCT
WHERE 	LISTPRICE > $25
AND 	LISTPRICE < $100;
GO

ALTER DATABASE ADVENTUREWORKS2017 
SET RECOVERY FULL;
GO

--NOTA: GO solo se utiliza en sqlcmd


/*Variables*/
--Utilizando SET--
DECLARE @NOMBRE VARCHAR(100)
    --la consulta debe devolver un unico registro
SET @NOMBRE = (SELECT NOMBRE FROM CLIENTES WHERE ID = 1);

--Utilizando SELECT--
DECLARE 
    @NOMBRE VARCHAR(100)
    @APELLIDO1 VARCHAR(100),
    @APELLIDO2 VARCHAR(100)
SELECT 
    @NOMBRE=NOMBRE, 
    @APELLIDO1=Apellido1, 
    @APELLIDO2=Apellido2
FROM 
    CLIENTES WHERE ID = 1;


/*Consultas*/
--General--
SELECT 		*
FROM		PRODUCTION.PRODUCT --Production. indica un esquema
ORDER BY 	NAME ASC; --ASC=ascendente

SELECT 
    * , 
    CAMPO1,
    CAMPO2, 
    CAMPO3,
    CAMPO4
FROM 
    TABLA,
    TABLA2,
    TABLA3,
    TABLA4,
    TABLA5
WHERE
    CONDICIONES --(FILTROS)
GROUP BY 
    --AGRUPADOS OPCIONAL
HAVING 
    --GRUPO MAS PRECISO DEL AGRUPADO OPCIONAL
ORDER BY 
    --MOSTRAR LA INFORMACION DE MANERA ORDENADA OPCIONAL

SELECT	
    ID, 
    FECHAEMISION,
    CLIENTE
FROM
    VENTA
WHERE 
    CLIENTE IN ('CL-2012704','CL-2003106','CL-2003614')
ORDER BY 
    CLIENTE;

--General con identificador--
SELECT 		P.*
FROM 		PRODUCTION.PRODUCT AS P --Puede ir sin el AS 
ORDER BY 	NAME ASC;

--DISTINCT-- (Devuelve los valores unicos de una consulta)
SELECT DISTINCT JOBTITLE
FROM 		HUMANRESOURCES.EMPLOYEE
ORDER BY 	JOBTITLE ASC;

--HAVING-- ( Agrupacion de datos mas especifico )
SELECT PRODUCTID
FROM SALES.SALESORDERDETAIL
GROUP BY PRODUCTID
HAVING AVG(ORDERQTY) > 5
ORDER BY PRODUCTID;
GO

SELECT 
    SALESORDERID,
    CARRIERTRACKINGNUMBER
FROM 
    SALES.SALESORDERDETAIL
GROUP BY 
    SALESORDERID,
    CARRIERTRACKINGNUMBER
HAVING 
    CARRIERTRACKINGNUMBER LIKE '4BD'
ORDER BY
    SALESORDERID;
GO


/*Funciones de cadena*/

--CONCAT-- ( Concatena cadenas de caracteres )
SELECT 
    PERSONAL,
    CONCAT(APELLIDOPATERNO, ' ', APELLIDOMATERNO, ' ', NOMBRE) AS 'NOMBRE COMPLETO',
    TIPO
FROM
    PERSONAL;
--Nota: se puede usar + tambien para concatenar

--ASCII-- (Retorna el valor ASCII del primer caracter)
SELECT 
    PERSONAL, 
    ASCII(PERSONAL), 
    (APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE) AS 'NOMBRE COMPLETO', 
    ASCII(APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE), TIPO, 
    ASCII( TIPO ) 
FROM PERSONAL;

SELECT
    ARTICULO,
    ASCII(ARTICULO),
    DESCRIPCION1,
    ASCII(DESCRIPCION1),
    CATEGORIA,
    ASCII(CATEGORIA)
FROM
    ART

--LTRIM-- ( Elimina espacios en blanco al inicio de la cadena )
SELECT 
    ('     ' + PERSONAL + '    '),
    LTRIM('     ' + PERSONAL + '    '),
    ('   ' + APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE + '     ') AS 'NOMBRE COMPLETO',
    LTRIM('   ' + APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE + '     '), 
    ('     ' + TIPO + '     '),
    LTRIM('     ' + TIPO + '     ') 
FROM
    PERSONAL;

SELECT
    ('     ' + ARTICULO + '     '),
    LTRIM('     ' + ARTICULO + '     '),
    ('     ' + DESCRIPCION1 + '     '),
    LTRIM('     ' + DESCRIPCION1 + '     '),
    ('     ' + CATEGORIA + '     '),
    LTRIM('     ' + CATEGORIA + '     ')
FROM
    ART

--RTRIM-- ( Elimina espacios en blanco al inicio de la cadena )
SELECT 
    ('     ' + PERSONAL + '    '),
    RTRIM('     ' + PERSONAL + '    '),
    ('   ' + APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE + '     ') AS 'NOMBRE COMPLETO',
    RTRIM('   ' + APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE + '     '), 
    ('     ' + TIPO + '     '),
    RTRIM('     ' + TIPO + '     ') 
FROM
    PERSONAL;

SELECT
    ('     ' + ARTICULO + '     '),
    RTRIM('     ' + ARTICULO + '     '),
    ('     ' + DESCRIPCION1 + '     '),
    RTRIM('     ' + DESCRIPCION1 + '     '),
    ('     ' + CATEGORIA + '     '),
    RTRIM('     ' + CATEGORIA + '     ')
FROM
    ART

--SPACE-- ( Devuelve una cadena de espacios )
SELECT 
    PERSONAL,
    (SPACE(5) + PERSONAL + SPACE(5)),
    LTRIM(SPACE(5) + PERSONAL + SPACE(5)),
    (APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE) AS 'NOMBRE COMPLETO',
    (SPACE(5) + APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE + SPACE(5)) 
    LTRIM(SPACE(5) + APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE + SPACE(5)), 
    TIPO,
    (SPACE(5) + TIPO + SPACE(5)),
    LTRIM(SPACE(5) + TIPO + SPACE(5)) 
FROM
    PERSONAL;

SELECT
    ARTICULO,
    (SPACE(5) + ARTICULO + SPACE(5)),
    LTRIM(SPACE(5) + ARTICULO + SPACE(5)),
    DESCRIPCION1,
    (SPACE(5) + DESCRIPCION1 + SPACE(5)),
    LTRIM(SPACE(5) + DESCRIPCION1 + SPACE(5)),
    CATEGORIA,
    (SPACE(5) + CATEGORIA + SPACE(5)),
    LTRIM(SPACE(5) + CATEGORIA + SPACE(5))
FROM
    ART

--STR-- ( Convierte un numero a tipo caracter )

SELECT
    ARTICULO,
    STR(ASCII(ARTICULO)),
    DESCRIPCION1,
    STR(ASCII(DESCRIPCION1)),
    CATEGORIA,
    STR(ASCII(CATEGORIA))
FROM
    ART


--STUFF-- (Inserta una cadena dentro de otra)
SELECT 
    PERSONAL, 
    (APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE) AS 'NOMBRE COMPLETO',
    TIPO,
    STUFF(PERSONAL,1,0,TIPO) AS 'PERSONAL + TIPO'
FROM 
    PERSONAL;

SELECT
    ARTICULO,
    DESCRIPCION1,
    CATEGORIA,
    STUFF(ARTICULO,8,10,(DESCRIPCION1 + ' ' + CATEGORIA))
FROM
    ART;

--REPLACE-- ( Reemplaza una cadena con otra )
SELECT 
    PERSONAL,
    CONCAT(APELLIDOPATERNO, ' ', APELLIDOMATERNO, ' ', NOMBRE) AS 'NOMBRE COMPLETO',
    TIPO,
    REPLACE( CONCAT(PERSONAL, ' ', APELLIDOPATERNO, ' ', APELLIDOMATERNO, ' ', NOMBRE, ' ', TIPO),' ','' ) AS 'SIN ESPACIOS'
FROM 
    PERSONAL;

--LEFT/RIGHT-- (Obtiene caracteres de la izquierda/derecha de una cadena)
SELECT 
    PERSONAL,
    LEFT(PERSONAL,5),
    CONCAT(APELLIDOPATERNO, ' ', APELLIDOMATERNO, ' ', NOMBRE) AS 'NOMBRE COMPLETO',
    LEFT(CONCAT(APELLIDOPATERNO, ' ', APELLIDOMATERNO, ' ', NOMBRE),5),
    TIPO,
    LEFT(TIPO,5)
FROM 
    PERSONAL;

SELECT
    ARTICULO,
    RIGHT(ARTICULO,5),
    DESCRIPCION1,
    RIGHT(DESCRIPCION1,5),
    CATEGORIA,
    RIGHT(CATEGORIA,5)
FROM
    ART

--SUBSTRING-- ( Extrae una cantidad de caracteres de una cadena )
SELECT 
    PERSONAL,
    SUBSTRING(PERSONAL,5,5),
    CONCAT(APELLIDOPATERNO, ' ', APELLIDOMATERNO, ' ', NOMBRE) AS 'NOMBRE COMPLETO',
    SUBSTRING(CONCAT(APELLIDOPATERNO, ' ', APELLIDOMATERNO, ' ', NOMBRE),5,5),
    TIPO,
    SUBSTRING(TIPO,5,5)
FROM 
    PERSONAL;

SELECT
    ARTICULO,
    SUBSTRING(ARTICULO,5,5),
    DESCRIPCION1,
    SUBSTRING(DESCRIPCION1,5,5),
    CATEGORIA,
    SUBSTRING(CATEGORIA,5,5)
FROM
    ART

--REPLICATE-- ( Repite una cadena de caracteres )
SELECT REPLICATE( PERSONAL + ' ',3 )
FROM PERSONAL;

SELECT REPLICATE( ARTICULO + ' ',3 )
FROM ART;

--REVERSE-- ( Invierte la escritura de una cadena )
SELECT 
    PERSONAL, 
    REVERSE(PERSONAL), 
    (APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE) AS 'NOMBRE COMPLETO', 
    REVERSE(APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE), TIPO, 
    REVERSE( TIPO ) 
FROM PERSONAL;

SELECT
    ARTICULO,
    REVERSE(ARTICULO),
    DESCRIPCION1,
    REVERSE(DESCRIPCION1),
    CATEGORIA,
    REVERSE(CATEGORIA)
FROM
    ART

--LOWER/UPPER-- ( Convierte a minusculas/mayusculas )
SELECT 
    PERSONAL, 
    LOWER(PERSONAL), 
    (APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE) AS 'NOMBRE COMPLETO', 
    LOWER(APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE), TIPO, 
    LOWER( TIPO ) 
FROM PERSONAL;

SELECT
    GRUPO,
    UPPER(GRUPO),
    CATEGORIA,
    UPPER(CATEGORIA)
FROM
    ART

--LEN-- ( Obtiene la longitud de cadena )
SELECT 
    PERSONAL, 
    LEN(PERSONAL), 
    (APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE) AS 'NOMBRE COMPLETO', 
    LEN(APELLIDOPATERNO + ' ' + APELLIDOMATERNO + ' ' + NOMBRE), TIPO, 
    LEN( TIPO ) 
FROM PERSONAL;

SELECT
    ARTICULO,
    LEN(ARTICULO),
    DESCRIPCION1,
    LEN(DESCRIPCION1),
    CATEGORIA,
    LEN(CATEGORIA)
FROM
    ART


--REPLACE--
SELECT 
    REPLACE('SQL SERVER MANUAL PROFESIONAL',' ',''); --reemplaza los caracteres de espacio por una cadena sin espacios

--SUBSTRING--
SELECT UPPER(SUBSTRING(REPLACE('DEL CASTILLO',' ',''),4,1)); --cuarta letra del apellido en mayuscula

--REVERSE--
SELECT REVERSE('UN GESTOR DE BASE DE DATOS'); --invierte la cadena de caracteres
EVERSE('UN GESTOR DE BASE DE DATOS'); --invierte la cadena de caracteres

--STUFF--
SELECT STUFF('FUNCIONES DE TEXTO Y CADENA',14,5,'FECHAS Y HORAS'); --reemplazo e insercion de caracteres

--REPLICATE--
SELECT REPLICATE('GOL... ',20); --repite una cadena un determinado numero de veces

--ASCII--
SELECT ASCII('AUTENTICO'); --devuelve el valor ascii del primer caracter

--LEN--
SELECT LEN(LTRIM(' HOLA ')); --obtiene la longitud de la cadena despues de eliminar los espacios en blanco de la izquierda

--Concatenacion de cadenas--
SELECT 
    UPPER('ALBERTO DEBE ') + 
    LTRIM(STR(800)) + 
    SPACE(1) + 
    LOWER('DOLARES');

--CONCAT--
SELECT 
CONCAT
(
    UPPER('ALBERTO DEBE '),
    LTRIM(STR(800)),
    SPACE(1),
    LOWER('DOLARES')
);


--COMANDOS CON CADENAS--

SELECT
    REVERSE(NOMBRE),
    REPLICATE(NOMBRE,2),
    LEFT(NOMBRE,10),
    STR(13),
    CLIENTE + '-' + NOMBRE, 
    LEN(CLIENTE + '-' + NOMBRE),
    REPLACE(REPLACE(REPLACE(LOWER
	(SUBSTRING(CLIENTE + '-' + NOMBRE,5,LEN(CLIENTE + '-' + NOMBRE))), 'LA','*'), 'E', 'EJEMPLO'), 'PI', '5'),
    SUBSTRING(CLIENTE + '-' + NOMBRE,1,7),
    '   ' + CLIENTE + '-' + NOMBRE + '   ',
    LTRIM('   ' + CLIENTE + '-' + NOMBRE + '   '),
    RTRIM('   ' + CLIENTE + '-' + NOMBRE + '   '),
    LTRIM(RTRIM('   ' + CLIENTE + '-' + NOMBRE + '   ')),
    LEN(LTRIM(RTRIM('   ' + CLIENTE + '-' + NOMBRE + '   ')))
FROM CTE


/*Funciones de Agregado*/

--AVG-- ( Devuelve el promedio de los valores )

--MIN-- ( Devuelve el valor minimo )

--SUM-- ( Suma los valores )

--COUNT-- ( Cuenta la cantidad de celdas )

--STDEV-- ( Devuelve la desviacion estandar Muestral )

--STDEVP-- ( Devuelve la desviacion estandar Poblacional )

--VAR-- ( Devuleve la varizza muestral )

--VARP-- ( Devualeve la varianza poblacional )

--MAX-- ( Devuelve el valor maximo )


/*Funciones de Fecha y Hora*/

--GETDATE Y SYSDATETIME--
SELECT
    GETDATE() AS 'FECHA ACTUAL', --FECHA Y HORA DEL SERVIDOR
    SYSDATETIME AS 'FECHA SISTEMA'; --OBTIENE LA FECHA DEL SERVIDOR

--DATENAME--
SELECT 
    DATENAME(YY,GETDATE()) AS AÑO, --año de la fecha
    DATENAME(MM,GETDATE()) AS MES, --nombre del mes
    DATENAME(DW,GETDATE()) AS DIA; --nombre del dia de la semana 
    
SELECT
    DATENAME(DD,GETDATE()) AS 'DIA DEL MES',
    DATENAME(DY,GETDATE()) AS 'DIA DEL AÑO';

--DATEADD--
SELECT DATEADD(D,60,GETDATE()); --agrega 60 dias a la fecha dada

--DATEPART--
SELECT
    DATEPART(WK,'1996/11/16') AS SEMANA, --no. de semana de la fecha dada
    DATEPART(DW,'2000/10/01') AS DIA; --no. de dia de la semana dada

--Partes de fecha--
year 		yy,yyyy
quarter		qq,q
month		mm,m
dayofyear	dy,y
day		dd,d
week		wk,ww
weekday		dw 
hour 		hh
minute		mi,n
second		ss,s



