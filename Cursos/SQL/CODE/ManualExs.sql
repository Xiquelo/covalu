/* Ejercicios del Manual*/

--Seleccion sencilla sin WHERE--
SELECT 		NAME, PRODUCTNUMBER, LISTPRICE PRICE
FROM 		PRODUCTION.PRODUCT
ORDER BY 	NAME ASC;

--Seleccion con WHERE--
SELECT 		NAME, PRODUCTNUMBER, LISTPRICE PRICE
FROM 		PRODUCTION.PRODUCT
WHERE 		PRODUCTLINE='R'
AND 		DAYSTOMANUFACTURE < 4
ORDER BY 	NAME ASC;

--Seleccion con encabezados y calculos--
SELECT 
    P.NAME AS PRODUCTNAME,
    NONDISCOUNTSALES = (ORDERQTY * UNITPRICE),
    DISCOUNTS = ((ORDERQTY * UNITPRICE) * UNITPRICEDICSCOUNT)
FROM 
    PRODUCTION.PRODUCT AS P
INNER JOIN 
    SALES.SALESORDERDETAIL AS SOD
    ON P.PRODUCT=SOD.PRODUCTID
ORDER BY
    PRODUCTNAME DESC;

SELECT 
    'Total income is', 
    ((ORDERQTY * UNITPRICE) * (1.0 - UNITPRICEDISCOUNT)), 
    ' for ',
    P.NAME AS PRODUCTNAME
FROM 
    PRODUCTION.PRODUCT AS P
INNER JOIN
    SALES.SALESORDERDETAIL AS SOD
    ON P.PRODUCTID = SOD.PRODUCTID
ORDER BY PRODUCTNAME ASC;


--Subconsultas--
USE ADVENTUREWORKS2017;
GO
SELECT DISTINCT NAME
FROM PRODUCTION.PRODUCT AS P
WHERE EXISTS
(
    SELECT 	*
    FROM 	Production.ProductModel AS PM 
    WHERE 	p.ProductModelID = pm.ProductModelID
    AND 	pm.Name LIKE 'Long-Sleeve Logo Jersey%'
);
GO

--OR


USE AdventureWorks2017;
GO
SELECT DISTINCT Name
FROM Production.Product
WHERE ProductModelID IN
(
    SELECT 	ProductModelID 
    FROM 	Production.ProductModel
    WHERE 	Name LIKE 'Long-Sleeve Logo Jersey%'
);
GO

USE ADVENTUREWORKS2017:
GO
SELECT DISTINCT 
    P.LASTNAME, 
    P.FIRSTNAME
FROM 
    PERSON.PERSON AS P
JOIN 
    HUMANRESOURCES.EMPLOYEE AS E
ON 
    E.BUSINESSENTITYID = P.BUSINESSENTITYID WHERE 5000.00 IN
(
    SELECT BONUS
    FROM SALES.SALESPERSON AS SP
    WHERE E.BUSINESSENTITYID = SP.BUSINESSENTITYID
);
GO

select distinct 
    pp.lastname,
    pp.firstname
from
    person.person pp 
join 
    humanresources.employee 
    on e.businessentityid = pp.businessentityid
where pp.businessentityid in
(
    select 
	salespersonid
    from
	sales.salesorderheader
    where
	salesorderid in
	(
	    select salesorderid
	    
	)
)
