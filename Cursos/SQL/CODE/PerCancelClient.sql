-- Cursor que indique el porcentaje de ventas canceladas con respecto al total de ventas por cliente
DECLARE
    @CLIENTE    VARCHAR( 10 ),
    @NOMBRE     VARCHAR( 50 ),
    ---------------------------
    @CANCEL     decimal( 6,5 )
    -- @TOTAL      decimal( 6,5 )

DECLARE C_CTE CURSOR LOCAL FOR
SELECT CLIENTE, NOMBRE FROM CTE

OPEN C_CTE
FETCH NEXT FROM C_CTE INTO @CLIENTE, @NOMBRE

WHILE @@FETCH_STATUS = 0
    BEGIN

        SET @CANCEL =
        (
            SELECT COUNT( ESTATUS )
            FROM VENTA 
            WHERE 
                CLIENTE = @CLIENTE
                    AND YEAR( FECHAEMISION ) = 2022
                    AND ESTATUS = 'CANCELADO'
        )

        -- SET @TOTAL =
        -- (
            -- SELECT COUNT( ESTATUS )
            -- FROM VENTA
        -- )

        IF @CANCEL > 0
            BEGIN
                SELECT 
                    @NOMBRE 'NOMBRE CLIENTE', 
                    (
                        CONVERT( FLOAT, @CANCEL  ) / 
                            CONVERT( FLOAT, COUNT( ESTATUS ) )
                    ) * 100 'PORCENTAJE DE CANCELADOS'
                FROM VENTA
                WHERE 
                    YEAR( FECHAEMISION ) = 2022
            END

        FETCH NEXT FROM C_CTE INTO @CLIENTE, @NOMBRE
    END

CLOSE C_CTE
DEALLOCATE C_CTE

