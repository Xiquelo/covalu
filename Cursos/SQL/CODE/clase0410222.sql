DECLARE
    @ANIO       INT,
    @MES        INT,
    @ESTATUS    VARCHAR(15),
    --------------------------
    @ENERO      DECIMAL(20,2),
    @FEBRERO    DECIMAL(20,2),
    @MARZO      DECIMAL(20,2),
    @ABRIL      DECIMAL(20,2),
    @MAYO       DECIMAL(20,2),
    @JUNIO      DECIMAL(20,2),
    @JULIO      DECIMAL(20,2),
    @AGOSTO     DECIMAL(20,2),
    @SEPTIEMBRE DECIMAL(20,2),
    @OCTUBRE    DECIMAL(20,2),
    @NOVIEMBRE  DECIMAL(20,2),
    @DICIEMBRE  DECIMAL(20,2),
    --------------------------
    @CONTEO     INT,
    @MESL       VARCHAR(15),
    @IMPORTEL   DECIMAL(12,2)

BEGIN
    
    SET LANGUAGE 'SPANISH'

    DROP TABLE #XPCOVVENTAS

    --- CREACION DE TABLA TEMPORAL PARA VENTAS
    CREATE TABLE #XPCOVVENTAS
    (
        CONSECUTIVO INT,
        MOVID       VARCHAR(20),
        ANIO        INT,
        MES         VARCHAR(15),
        ESTATUS     VARCHAR(20),
        IMPORTE     DECIMAL(14,2)
    )

    --- LLENADO DE TABLA DETALLE
    INSERT INTO #XPCOVVENTAS 
        SELECT 
            ROW_NUMBER() OVER(ORDER BY MOVID ASC), 
            MOVID, ANIO, MES, ESTATUS, 
            PRECIO
        FROM    
        ( 
            SELECT    
                V.MOVID MOVID,
                YEAR(V.FECHAEMISION) ANIO,
                DATENAME(MONTH,V.FECHAEMISION) MES,
                ESTATUS ESTATUS,
                (
                    CASE 
                        WHEN ESTATUS ='CANCELADO' 
                            THEN SUM(VD.CANTIDAD * VD.PRECIO) * -1
                        WHEN ESTATUS ='CONCLUIDO' THEN SUM(VD.CANTIDAD * VD.PRECIO)
                    ELSE 0 END
                ) 
                PRECIO
            FROM VENTA V 
            INNER JOIN 
                VENTAD VD ON V.ID = VD.ID
            WHERE V.MOV LIKE 'FACT%'
            GROUP BY 
                V.MOVID,
                YEAR(V.FECHAEMISION),
                DATENAME(MONTH,V.FECHAEMISION),
                ESTATUS
        ) A
        ORDER BY 2

    SET @CONTEO = (SELECT COUNT(1) FROM #XPCOVVENTAS)
    SET @ENERO = 0
    SET @FEBRERO = 0
    SET @MARZO = 0
    SET @ABRIL = 0
    SET @MAYO = 0
    SET @JUNIO = 0
    SET @JULIO = 0
    SET @AGOSTO = 0
    SET @SEPTIEMBRE = 0
    SET @OCTUBRE = 0
    SET @NOVIEMBRE = 0
    SET @DICIEMBRE = 0

    WHILE @CONTEO > 0
        BEGIN

            SELECT    
                @MESL = MES,
                @IMPORTEL = IMPORTE 
                FROM #XPCOVVENTAS 
                WHERE CONSECUTIVO = @CONTEO
            --SELECT @MESL, @IMPORTEL
            
            ---- VALIDAR EL MES, PARA AGRUPAR
            IF LTRIM(RTRIM(@MESL)) ='ENERO'
                BEGIN
                    SET @ENERO = (@ENERO + @IMPORTEL)
                    --SELECT @ENERO
                END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='FEBRERO'
                    BEGIN
                        SET @FEBRERO = (@FEBRERO + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='MARZO'
                    BEGIN
                        SET @MARZO = (@MARZO + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='ABRIL'
                    BEGIN
                        SET @ABRIL = (@ABRIL + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='MAYO'
                    BEGIN
                        SET @MAYO = (@MAYO + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='JUNIO'
                    BEGIN
                        SET @JUNIO = (@JUNIO + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='JULIO'
                    BEGIN
                        SET @JULIO = (@JULIO + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='AGOSTO'
                    BEGIN
                        SET @AGOSTO = (@AGOSTO + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='SEPTIEMBRE'
                    BEGIN
                        SET @SEPTIEMBRE = (@SEPTIEMBRE + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='OCTUBRE'
                    BEGIN
                        SET @OCTUBRE = (@OCTUBRE + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='NOVIEMBRE'
                    BEGIN
                        SET @NOVIEMBRE = (@NOVIEMBRE + @IMPORTEL)
                    END 
            ELSE
                IF LTRIM(RTRIM(@MESL)) ='DICIEMBRE'
                    BEGIN
                        SET @DICIEMBRE = (@DICIEMBRE + @IMPORTEL)
                    END 

            SET @CONTEO = (@CONTEO - 1)
        END 

    /******SEPARACIÓN DE INFORMAICÓN POR MES***/

    SELECT 'EL MONTO DE ENERO ES :', @ENERO
    SELECT 'EL MONTO DE FEBRERO ES :', @FEBRERO
    SELECT 'EL MONTO DE MARZO ES :', @MARZO
    SELECT 'EL MONTO DE ABRIL ES :', @ABRIL
    SELECT 'EL MONTO DE MAYO ES :', @MAYO
    SELECT 'EL MONTO DE JUNIO ES :', @JUNIO
    SELECT 'EL MONTO DE JULIO ES :', @JULIO
    SELECT 'EL MONTO DE AGOSTO ES :', @AGOSTO
    SELECT 'EL MONTO DE SEPTIEMBRE ES :', @SEPTIEMBRE
    SELECT 'EL MONTO DE OCTUBRE ES :', @OCTUBRE
    SELECT 'EL MONTO DE NOVIEMBRE ES :', @NOVIEMBRE
    SELECT 'EL MONTO DE DICIEMBRE ES :', @DICIEMBRE

END
