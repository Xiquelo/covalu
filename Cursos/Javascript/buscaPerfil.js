// Configuración
const contacts = [
  {
    firstName: "Akira",
    lastName: "Laine",
    number: "0543236543",
    likes: ["Pizza", "Coding", "Brownie Points"],
  },
  {
    firstName: "Harry",
    lastName: "Potter",
    number: "0994372684",
    likes: ["Hogwarts", "Magic", "Hagrid"],
  },
  {
    firstName: "Sherlock",
    lastName: "Holmes",
    number: "0487345643",
    likes: ["Intriguing Cases", "Violin"],
  },
  {
    firstName: "Kristian",
    lastName: "Vos",
    number: "unknown",
    likes: ["JavaScript", "Gaming", "Foxes"],
  },
];

function lookUpProfile(name, prop) {
  // Cambia solo el código debajo de esta línea
    for ( n in contacts ) {
        if ( name === contacts[n].firstName && contacts[n].hasOwnProperty( prop ) === true ) {
            return contacts[n][prop];
        }
        else if ( name === contacts[n].firstName && contacts[n].hasOwnProperty( prop ) !== true ) {
            return "No such property";
        }
        else if ( name !== contacts[n].firstName && n == contacts.length - 1 ) {
            return "No such contact";
        }
    }
  // Cambia solo el código encima de esta línea
}
//console.log (contacts[1].hasOwnProperty( "likes" ));
console.log( lookUpProfile("Bob", "likes") );
//console.log( contacts[1].likes )
//lookUpProfile("Akira", "likes");
