//Array Inicial
const lista = [ true, 5, false, "hola", "adios", 2, true, false ];
const i = lista.values();
let str1;
let str2;
const num = [];
let cont = 0;

for ( const valor of i )
{
    if ( typeof( valor ) === "string" )   
    {
        if ( str1 === undefined )
            str1 = valor;
        else
            str2 = valor;
    }

    if ( typeof( valor ) === "boolean" )
        cont += 1;

    if ( typeof( valor ) === "number" )   
        num.push( valor );
}

//Determinar cual de los elementos de texto tiene la cadena mas larga
console.log( `Que elemento de tipo string tiene la cadena mas larga?` );
if ( str1.length > str2.length )
    console.log( `La palabra "${str1}" tiene la cadena mas larga\n` );
else
    console.log( `La palabra "${str2}" tiene la cadena mas larga\n` );

//Cuantos elementos booleanos existen
console.log( `Cuantos elementos booleanos existen?` );
console.log( `${cont}\n` );

//Utilizando los dos valores booleanos determinar los operadores necesarios para obtener un resultado true-false

//Realizar las 5 operaciones matematicas con los dos elementos numericos
console.log( `Las 5 operaciones matematicas basicas sobre los elementos numericos` );
console.log( `${num.at(0)} + ${num.at(1)} = ${num.at(0) + num.at(1)}` );
console.log( `${num.at(0)} - ${num.at(1)} = ${num.at(0) - num.at(1)}` );
console.log( `${num.at(0)} * ${num.at(1)} = ${num.at(0) * num.at(1)}` );
console.log( `${num.at(0)} / ${num.at(1)} = ${num.at(0) / num.at(1)}` );
console.log( `${num.at(0)} ^ ${num.at(1)} = ${num.at(0) ** num.at(1)}` );
